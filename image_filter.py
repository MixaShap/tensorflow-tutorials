from skimage import data
from skimage.exposure import rescale_intensity
import matplotlib.pyplot as plt

from skimage.color.adapt_rgb import adapt_rgb
from skimage import filters
from skimage.color import rgb2gray


def as_gray(image_filter, image, *args, **kwargs):
    gray_image = rgb2gray(image)
    return image_filter(gray_image, *args, **kwargs)


@adapt_rgb(as_gray)
def sobel_gray(image):
    return filters.sobel(image)


image = data.astronaut()
fig, ax = plt.subplots(ncols=1, nrows=1, figsize=(5, 5))

# We use 1 - sobel_gray(image) but this won't work if image is not normalized
ax.imshow(rescale_intensity(1 - sobel_gray(image)), cmap='gray')
ax.set_xticks([]), ax.set_yticks([])
ax.set_title("Sobel filter computed\n on the converted grayscale image")

plt.show()
