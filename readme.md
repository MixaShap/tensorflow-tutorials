Архив с дорожными знаками можно скачать отсюда: https://btsd.ethz.ch/shareddata/  <br />
Использовалась версия TensorFlow 1.15.0  <br />
Файл jupyter notebook lesson3 анализирует дорожные знаки на основе датасета  <br />
Файл jupyter notebook neural_sign_language анализирует алфавит языка жестов на основе датасета